//
//  CodingTab.cpp
//  MidiScript
//
//  Created by Adam Bethell on 02/08/2017.
//
//

#include "CodingTab.hpp"

CodingTab::CodingTab (ScriptStore* scriptStorePointer) :
editor (document),
colourSelector (ColourSelector::showColourspace),
scriptStore (scriptStorePointer)
{
    scriptStoreId = scriptStore->registerNewScript();
    
    
    
    addAndMakeVisible(&inputSourceBox);
    StringArray devices = MidiInput::getDevices();
    devices.removeString("from MidiScript");
    inputSourceBox.addItemList(devices, 1);
    inputSourceBox.addItemList(GlobalVariableStore::GetInstance()->getMetros(), inputSourceBox.getNumItems()+1);
    inputSourceBox.addItem("to MidiScript", inputSourceBox.getNumItems()+1);
    inputSourceBox.setSelectedItemIndex(0);
    addMouseListener(this, true);
    
    addAndMakeVisible(&inputChannelBox);
    inputChannelBox.addItem("All Channels", 1);
    for (int i = 0; i < 16; i++)
    {
        inputChannelBox.addItem(String(i+1), i+2);
    }
    inputChannelBox.setSelectedItemIndex(0);
    
    
    addAndMakeVisible(&colourSelector);
    addAndMakeVisible(&colourSelectedButton);
    colourSelectedButton.setButtonText("Set Colour");
    colourSelectedButton.addListener(this);
    addAndMakeVisible(&tabName);
    tabName.addListener(this);
    tabName.setText("Untitled", dontSendNotification);
    
    
    addAndMakeVisible(&editor);
    
    addAndMakeVisible(&shouldSendNote);
    shouldSendNote.setButtonText("shouldOutput");
    shouldSendNote.setToggleState(true, dontSendNotification);
    
    
    addAndMakeVisible(&outputLabel);
    outputLabel.setText("outputDelay (ms)", dontSendNotification);
    addAndMakeVisible(&outputDelay);
    outputDelay.setSliderStyle(Slider::SliderStyle::IncDecButtons);
    outputDelay.setRange(0, 10000, 1);
    
    
    
    
    addAndMakeVisible(&pushCodeButton);
    pushCodeButton.setButtonText("<--- Push Code To Processor --->");
    pushCodeButton.addListener(this);
}
CodingTab::~CodingTab()
{
    scriptStore->unregisterScript(scriptStoreId);
}
void CodingTab::resized ()
{
    Rectangle<int> bounds = Rectangle<int>(0, 0, getWidth(), getHeight());
    bounds.reduce(5, 5);
    int quaterWidth = bounds.getWidth() / 4;
    Rectangle<int> buttonArea = bounds.removeFromTop(50);
    
    pushCodeButton.setBounds(bounds.removeFromBottom(50));
    Rectangle<int> outputArea = bounds.removeFromBottom(20);
    
    int quarter = outputArea.getWidth()/4;
    outputDelay.setBounds(outputArea.removeFromRight(quarter));
    outputLabel.setBounds(outputArea.removeFromRight(quarter));
    shouldSendNote.setBounds(outputArea);
    
    editor.setBounds(bounds);
    inputSourceBox.setBounds(buttonArea.removeFromLeft(quaterWidth).reduced(5));
    inputChannelBox.setBounds(buttonArea.removeFromLeft(quaterWidth).reduced(5));
    colourSelector.setBounds(buttonArea.removeFromLeft(quaterWidth).reduced(1));
    buttonArea.reduce(2, 2);
    colourSelectedButton.setBounds(buttonArea.removeFromBottom(buttonArea.getHeight()/2));
    tabName.setBounds(buttonArea);
}

void CodingTab::buttonClicked (Button* button)
{
    if (button == &colourSelectedButton)
    {
        if (listener != nullptr)
        {
            listener->newColourSelected(colourSelector.getCurrentColour());
        }
    }
    else if (button == &pushCodeButton)
    {
        String s = String::formatted("shouldOutput = %d\noutputDelay = %d\n", shouldSendNote.getToggleState(), (int)outputDelay.getValue()) + document.getAllContent();
        scriptStore->updateScript(scriptStoreId, s);
        scriptStore->updateSource(scriptStoreId, inputSourceBox.getText(), inputChannelBox.getSelectedItemIndex());
    }
    
}

void CodingTab::textEditorTextChanged (TextEditor& textEditor)
{
    if (&textEditor == &tabName)
    {
        if (listener != nullptr)
        {
            listener->newTabNameChosen(tabName.getText());
        }
    }
}

void CodingTab::mouseDown (const MouseEvent& event)
{
    StringArray devices = MidiInput::getDevices();
    devices.removeString("from MidiScript");
    
    String s = inputSourceBox.getText();
    
    inputSourceBox.clear();
    inputSourceBox.addItemList(devices, 1);
    inputSourceBox.addItemList(GlobalVariableStore::GetInstance()->getMetros(), inputSourceBox.getNumItems()+1);
    inputSourceBox.addItem("to MidiScript", inputSourceBox.getNumItems()+1);
    
    if (devices.contains(s) || GlobalVariableStore::GetInstance()->getMetros().contains(s) || s == "to MidiScript")
    {
        for (int i = 0; i < inputSourceBox.getNumItems(); i++)
        {
            if (s == inputSourceBox.getItemText(i))
            {
                inputSourceBox.setSelectedItemIndex(i);
                break;
            }
        }
    }
    else
    {
        int id = inputSourceBox.getNumItems()+1;
        inputSourceBox.addItem(s, id);
        inputSourceBox.setSelectedId(id);
        inputSourceBox.setItemEnabled(id, false);
    }
}

void CodingTab::handleMessage (const Message& message)
{
    
}
