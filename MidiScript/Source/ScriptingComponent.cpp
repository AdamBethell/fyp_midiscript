//
//  ScriptingComponent.cpp
//  MidiScript
//
//  Created by Adam Bethell on 28/07/2017.
//
//

#include "ScriptingComponent.hpp"

ScriptingComponent::ScriptingComponent()
{
    midiInput = MidiInput::createNewDevice("to MidiScript", this);
    midiInput->start();
    
    midiOutput = MidiOutput::createNewDevice("from MidiScript");
    
    addAndMakeVisible(&liveCodingComponent);
    liveCodingComponent.setListener(this);
    
    addAndMakeVisible(&staticVariablesComponent);
    staticVariablesComponent.setListener(this);
    
    addAndMakeVisible(&metronome);
     metronome.setListener(this);
    
//    addAndMakeVisible(&newPageButton);
//    newPageButton.setButtonText("+new");
//    newPageButton.addListener(this);
//    addAndMakeVisible(&codeWizardButton);
//    codeWizardButton.setButtonText("wiz");
//    codeWizardButton.addListener(this);
//    addAndMakeVisible(&zoomInButton);
//    zoomInButton.setButtonText("+zoom");
//    zoomInButton.addListener(this);
//    addAndMakeVisible(&zoomOutButton);
//    zoomOutButton.setButtonText("-zoom");
//    zoomOutButton.addListener(this);
//    addAndMakeVisible(&deletePageButton);
//    deletePageButton.setButtonText("-del");
//    deletePageButton.addListener(this);
}

ScriptingComponent::~ScriptingComponent()
{
    audioDeviceManager.removeMidiInputCallback(String::empty, this);
}

void ScriptingComponent::paint (Graphics& g)
{
    g.fillAll (getLookAndFeel().findColour (ResizableWindow::backgroundColourId));
    
}

void ScriptingComponent::resized()
{
    
    
    
//    metronome.setBounds(getWidth()*0.25, 0, getWidth()*0.5, 25);
//    staticVariablesComponent.setBounds(0, 25, div1, getHeight()-25);
//    liveCodingComponent.setBounds(div1, 25, div2-div1, getHeight()-25);
}

void ScriptingComponent::buttonClicked (Button* button)
{
    
}

void ScriptingComponent::initStaticVars (String script)
{
    pythonHandler.runOnce(script);
}

void ScriptingComponent::scriptPushed (String script)
{
    // NOT THREAD SAFE
    pythonHandler.passScript(script);
}

void ScriptingComponent::handleIncomingMidiMessage (MidiInput* source, const MidiMessage& message)
{
    MidiMessage outputMessage = pythonHandler.processMessage(message, source->getName());
    midiOutput->sendMessageNow(outputMessage);
}

void ScriptingComponent::messageFromMetronome(String source)
{
    MidiMessage outputMessage = pythonHandler.processMessage(MidiMessage::noteOn(1, 0, (uint8)0), source);
    midiOutput->sendMessageNow(outputMessage);
}
