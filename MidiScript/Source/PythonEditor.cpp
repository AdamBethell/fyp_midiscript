//
//  PythonEditor.cpp
//  MidiScript
//
//  Created by Adam Bethell on 28/09/2017.
//
//

#include "PythonEditor.hpp"

PythonEditor::PythonEditor(CodeDocument& document) : juce::CodeEditorComponent(document, nullptr)
{
    calloutIsOpen = false;
    setTabSize(4, false);
    
}
PythonEditor::~PythonEditor()
{
    
}

void PythonEditor::handleEscapeKey()
{
    return;
    
    if (calloutIsOpen)
    {
        calloutIsOpen = false;
        autoCompleter = nullptr;
        autoSuggestionsBox = nullptr;
    }
    else
    {
        calloutIsOpen = true;
        autoCompleter = new AutoCompleter(getCurrentWord());
        autoCompleter->setListener(this);
        autoSuggestionsBox = new CallOutBox(*autoCompleter, getCaretRectangle(), this);
        
    }
}

void PythonEditor::handleReturnKey()
{
    
    // Start next line at same indentation level
    String lineString = getCaretPos().getLineText();
    int numTabs = 0;
    for (int i = 0; i < lineString.length(); i++)
    {
        if (lineString[i] == '\t')
        {
            numTabs++;
        }
    }
    
    insertTextAtCaret("\n");
    for (int i = 0; i < numTabs; i++) insertTabAtCaret();
    
}

void PythonEditor::updateCaretPosition()
{
    CodeEditorComponent::updateCaretPosition();
    if (true /*autoSuggestionsBox != nullptr && autoCompleter != nullptr*/)
    {
        autoSuggestionsBox = nullptr;
        autoCompleter = nullptr;
        autoCompleter = new AutoCompleter(getCurrentWord());
        autoCompleter->setListener(this);
        autoSuggestionsBox = new CallOutBox(*autoCompleter, getCaretRectangle(), this);
        autoSuggestionsBox->setVisible(true);
        autoSuggestionsBox->setMouseClickGrabsKeyboardFocus(false);
        
        if (!autoCompleter->foundCompletions())
            autoSuggestionsBox->setVisible(false);
    }
}

String PythonEditor::getCurrentWord()
{

    
//    DBG("Getting range: " << startPos << " - " << endPos);
    String result = getTextInRange (getCurrentWordRange());
    if (result == "")
    {
        return ";";
    }
    
    return result;
}
Range<int> PythonEditor::getCurrentWordRange()
{
//    DBG("");
    int endPos = getCaretPos().getPosition();
    int startPos = endPos;
    for (int i = endPos; i >= 0; i--)
    {
        String temp = getTextInRange (Range<int>(i-1, i));
//        DBG("Checking char: '" << temp << "' at: " << i);
        if (!getTextInRange (Range<int>(i-1, i)).containsOnly ("1234567890abcdefghijklmnopqrstuvwxyz_ABCDEFGHIJKLMNOPQRSTUVWXYZ") ||
            getTextInRange (Range<int>(i-1, i)) == "")
        {
//            DBG("Failed check");
            startPos = i;
            break;
        }
//        DBG("Passed check");
    }
    return Range<int>(startPos, endPos);
}

void PythonEditor::completionSelected (const String& completionCode)
{
    int length = getCurrentWordRange().getLength();
   
    for (int i = 0; i < length; i++)
    {
        moveCaretLeft(false, true);
    }
    
    insertFormattedTextAtClaret (completionCode);
}

void PythonEditor::insertFormattedTextAtClaret (String s)
{
    while (true)
    {
        int index = -1;
        int index2 = -1;
        
        index = s.indexOf("\n");
        index2 = s.indexOf("\b");
        if (index2 < index && index2 != -1)
        {
            if (index2 != -1)
            {
                insertTextAtCaret(s.substring (0, index2));
                deleteBackwards(false);
                s = s.substring(index2+1);
            }
            else
            {
                insertTextAtCaret(s);
                break;
            }
        }
        else
        {
            if (index != -1)
            {
                insertTextAtCaret(s.substring (0, index));
                handleReturnKey();
                s = s.substring(index+1);
            }
            else
            {
                insertTextAtCaret(s);
                break;
            }
        }
    }
    
}
