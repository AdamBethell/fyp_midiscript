//
//  GuiToggle.hpp
//  MidiScript
//
//  Created by Adam Bethell on 13/10/2017.
//
//

#ifndef GuiToggle_hpp
#define GuiToggle_hpp

#include "../JuceLibraryCode/JuceHeader.h"
#include "GuiComponent.cpp"

class GuiToggle :
public GuiComponent,
public Button::Listener,
public TextEditor::Listener
{
public:
    GuiToggle();
    ~GuiToggle();
    
    void paint (Graphics& g) override;
    void resized() override;
    
    void buttonClicked (Button* button) override;
    void textEditorTextChanged (TextEditor& editor) override;
    void handleMessage (const Message& message) override;
    
    void setID (int idNum) override { id = idNum; }
    String getVarName() override { return name; }
    double getVarValue() override { return (int)toggleButton.getToggleState(); }
    void setName (String newName) override;
private:
    ToggleButton toggleButton;
    TextEditor textEditor;
    String name;
    int id;
};

#endif /* GuiToggle_hpp */
