//
//  GuiValueMessage.hpp
//  MidiScript
//
//  Created by Adam Bethell on 13/10/2017.
//
//

#ifndef GuiValueMessage_hpp
#define GuiValueMessage_hpp

#include "../JuceLibraryCode/JuceHeader.h"

struct GuiValueMessage : public Message
{
public:
    double value;
};

#endif /* GuiValueMessage_hpp */
