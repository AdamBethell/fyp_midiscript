//
//  ScriptingAreaComponent.cpp
//  MidiScript
//
//  Created by Adam Bethell on 29/09/2017.
//
//

#include "ScriptingAreaComponent.hpp"

ScriptingAreaComponent::ScriptingAreaComponent (ScriptStore* scriptStorePointer, PythonProcessor* pythonProcessor) :
tabbedCodingArea(TabbedButtonBar::Orientation::TabsAtTop),
scriptStore(scriptStorePointer),
processor(pythonProcessor)
{
    jassert(THIS == nullptr);
    THIS = this;
    
    addAndMakeVisible(&tabbedCodingArea);
    addNewPage();
    
    addAndMakeVisible(&newTabButton);
    newTabButton.setButtonText("New Page");
    newTabButton.addListener(this);
    
    addAndMakeVisible(&deleteTabButton);
    deleteTabButton.setButtonText("Delete Page");
    deleteTabButton.addListener(this);
    
    addAndMakeVisible(&staticCode);
    staticCode.setListener(this);
}

ScriptingAreaComponent::~ScriptingAreaComponent()
{
    
}

ScriptingAreaComponent* ScriptingAreaComponent::THIS = nullptr;

void ScriptingAreaComponent::resized()
{
    Rectangle<int> bounds (getBounds());
    staticCode.setBounds (bounds.removeFromLeft(200));
    tabbedCodingArea.setBounds (bounds.getX(), bounds.getY(), bounds.getWidth(), bounds.getHeight()-50);
    Rectangle<int> buttonArea (bounds.getX(), bounds.getHeight()-50, bounds.getWidth(), 50);
    newTabButton.setBounds (buttonArea.removeFromLeft(bounds.getWidth()/2).reduced(1));
    deleteTabButton.setBounds (buttonArea.reduced(1));
}

void ScriptingAreaComponent::buttonClicked(Button* button)
{
    if (button == &newTabButton)
    {
        addNewPage();
    }
    else if (button == &deleteTabButton)
    {
        int currentTabIndex = tabbedCodingArea.getCurrentTabIndex();
        tabbedCodingArea.removeTab(currentTabIndex);
        codingTabs.remove(currentTabIndex);
        
        currentTabIndex--;
        if (tabbedCodingArea.getNumTabs() <= 0) {
            addNewPage();
            currentTabIndex = 0;
        }
        
        tabbedCodingArea.setCurrentTabIndex(currentTabIndex);
    }
}

void ScriptingAreaComponent::addNewPage()
{
    codingTabs.add(new CodingTab(scriptStore));
    CodingTab* cd = codingTabs.getLast();
    cd->setListener(this);
    tabbedCodingArea.addTab("Untitled", Colours::darkgrey, cd, false);
    
}

void ScriptingAreaComponent::newColourSelected (Colour colour)
{
    tabbedCodingArea.setTabBackgroundColour(tabbedCodingArea.getCurrentTabIndex(), colour);
}

void ScriptingAreaComponent::newTabNameChosen(String tabName)
{
    tabbedCodingArea.setTabName(tabbedCodingArea.getCurrentTabIndex(), tabName);
}

void ScriptingAreaComponent::initStaticVars (String script)
{
    processor->runOnce(script);
}
