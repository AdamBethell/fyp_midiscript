//
//  LiveCodingComponent.hpp
//  MidiScript
//
//  Created by Adam Bethell on 28/07/2017.
//
//

#ifndef LiveCodingComponent_hpp
#define LiveCodingComponent_hpp

#include "../JuceLibraryCode/JuceHeader.h"

class LiveCodingComponent : public Component, public Button::Listener
{
public:
    LiveCodingComponent();
    ~LiveCodingComponent() {}
    
    void buttonClicked (Button* button) override;
    void resized() override;
    
    class Listener
    {
    public:
        virtual ~Listener() {}
        virtual void scriptPushed (String script) = 0;
    };
    void setListener (Listener* newListener) { listener = newListener; }
private:
    Listener* listener;
    
    // The CodeDocument MUST be declared before the CodeEditorComponent
    CodeDocument codeDocument;
    CodeEditorComponent codeEditor;
    
    TextButton submitCodeButton;
    
    Label infoLabel;
};

#endif /* LiveCodingComponent_hpp */
