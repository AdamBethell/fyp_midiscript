//
//  PythonProcessor.cpp
//  MidiScript
//
//  Created by Adam Bethell on 04/08/2017.
//
//

#include "PythonProcessor.hpp"

PythonProcessor::PythonProcessor(ScriptStore* scriptStorePointer) :
scriptStore(scriptStorePointer)
{
    jassert(pp == nullptr);
    pp = this;
 
    startTimer(1000);
    
    MidiInput* input = MidiInput::createNewDevice("to MidiScript", this);
    input->start();
    audioDeviceManager.setMidiInputEnabled("to MidiScript", true);
    
    midiOutput = MidiOutput::createNewDevice("from MidiScript");
    midiOutput->startBackgroundThread();
    
    currentInputs = MidiInput::getDevices();
    for (String s : currentInputs)
    {
        audioDeviceManager.setMidiInputEnabled(s, true);
    }
    audioDeviceManager.addMidiInputCallback(String::empty, this);
    
    
    Py_Initialize();
    main = PyImport_ImportModule("__main__");
    dict = PyModule_GetDict(main);
    
    imports = "from random import randint\n";
}

PythonProcessor::~PythonProcessor()
{
    midiOutput->stopBackgroundThread();
    Py_Finalize();
    audioDeviceManager.removeMidiInputCallback(String::empty, this);
}

PythonProcessor* PythonProcessor::pp = nullptr;

void PythonProcessor::timerCallback()
{
    StringArray updatedInputs =  MidiInput::getDevices();
    
    if (currentInputs.size() != 0)
    {
        for (int i = currentInputs.size()-1; i >= 0; i--)
        {
            if (!updatedInputs.contains(currentInputs[i]))
            {
                DBG("Input: " << currentInputs[i] << " lost");
                audioDeviceManager.setMidiInputEnabled(currentInputs[i], false);
                currentInputs.remove(i);
            }
        }
    }
    
    if (updatedInputs.size() != 0)
    {
        for (int i = 0; i < updatedInputs.size(); i++)
        {
            if (!currentInputs.contains(updatedInputs[i]))
            {
                DBG("Input: " << updatedInputs[i] << " found");
                audioDeviceManager.setMidiInputEnabled(updatedInputs[i], true);
                currentInputs.add(updatedInputs[i]);
            }
        }
    }
    
}

void PythonProcessor::handleMessage (const Message& message)
{
    const MetroMidiMessage* mmm = dynamic_cast<const MetroMidiMessage*>(&message);
    if (mmm != nullptr)
    {
        messageHandler(mmm->source, mmm->message);
    }
    else jassertfalse; // NULLPOINTER
}

void PythonProcessor::handleIncomingMidiMessage (MidiInput* source, const MidiMessage& message)
{
    messageHandler(source->getName(), message);
}

void PythonProcessor::messageHandler (String sourceName, const MidiMessage& message)
{
    const ScopedLock lock (processorLock);
    
    
    if (!message.isNoteOnOrOff()) return;
    
    MidiBuffer buffer;
    if (message.isNoteOn())
    {
        
        StringArray scriptsToRun = scriptStore->getScriptsForSource(sourceName, message.getChannel());
        //    DBG("num found: " << scriptsToRun.size());
        
        
        for (int i = 0; i < scriptsToRun.size(); i++)
        {
            
            
            PyObject* noteNumber = PyInt_FromLong(message.getNoteNumber());
            PyObject* noteVelocity = PyInt_FromLong(message.getVelocity());
            
            PyDict_SetItemString(dict, "note", noteNumber);
            PyDict_SetItemString(dict, "velocity", noteVelocity);
            
            StringArray strings = GlobalVariableStore::GetInstance()->getStringArray();
            Array<double> values = GlobalVariableStore::GetInstance()->getValues();
            Array<unsigned int> ids = GlobalVariableStore::GetInstance()->getIds();
            
            populateDict (dict, strings, values);
            
            String s = imports + scriptsToRun[i];
            
            PyRun_SimpleString(s.toRawUTF8());
            
            updateFromDict (dict, strings, values, ids);
            
            noteNumber = PyMapping_GetItemString(dict, "note");
            noteVelocity = PyMapping_GetItemString(dict, "velocity");
            PyObject* shouldSend = PyMapping_GetItemString(dict, "shouldOutput");
            PyObject* delayAmount = PyMapping_GetItemString(dict, "outputDelay");
            
            int noteNum = _PyInt_AsInt(noteNumber);
            int noteVel = _PyInt_AsInt(noteVelocity);
            int send = _PyInt_AsInt(shouldSend);
            int delay = _PyInt_AsInt(delayAmount);
            
            if (send == 1)
            {
                if (noteNum > 127) noteNum = 127;
                else if (noteNum < 0) noteNum = 0;
                if (noteVel > 127) noteVel = 127;
                else if (noteVel < 0) noteVel = 0;
                
                
                MidiMessage newMessage(message);
                newMessage.setNoteNumber(noteNum);
                newMessage.setVelocity((float)noteVel/127.0);
                
                buffer.addEvent(newMessage, delay);
            }
        }
        
        midiLogger.logNote(message, buffer);
    }
    else
    {
        buffer = midiLogger.getOutputBufferForNoteOff(message);
    }
    
    midiOutput->sendBlockOfMessages(buffer, Time::getMillisecondCounter(), 1000.0);
}

void PythonProcessor::populateDict(PyObject* dict, StringArray strings, Array<double> values)
{
    for (int i = 0; i < strings.size(); i++)
    {
        PyObject* value = PyFloat_FromDouble(values[i]);
        PyDict_SetItemString(dict, strings[i].toRawUTF8(), value);
        Py_DecRef(value);
    }
}

void PythonProcessor::updateFromDict (PyObject* dict, StringArray strings, Array<double> values, Array<unsigned int> ids)
{
    for (int i = 0; i < strings.size(); i++)
    {
        
        PyObject* pValue = PyMapping_GetItemString(dict, (char*)strings[i].toRawUTF8());
        double value = PyFloat_AsDouble(pValue);
        
        GlobalVariableStore::GetInstance()->updateGlobal(ids[i], strings[i], value, true);
    }
    
}
