//
//  CodingTab.hpp
//  MidiScript
//
//  Created by Adam Bethell on 02/08/2017.
//
//

#ifndef CodingTab_hpp
#define CodingTab_hpp

#include "../JuceLibraryCode/JuceHeader.h"
#include "ScriptStore.hpp"
#include "PythonEditor.hpp"
#include "GlobalVariableStore.hpp"


class CodingTab : public Component, public Button::Listener, public TextEditor::Listener, public MessageListener
{
public:
    CodingTab(ScriptStore* scriptStorePointer);
    ~CodingTab();
    void resized () override;
    void buttonClicked (Button* button) override;
    void textEditorTextChanged (TextEditor& textEditor) override;
    void handleMessage (const Message& message) override;
    class Listener
    {
    public:
        virtual ~Listener(){}
        virtual void newColourSelected (Colour colour) = 0;
        virtual void newTabNameChosen (String tabName) = 0;
    };
    void setListener (Listener* newListener) { listener = newListener; }
    void mouseDown (const MouseEvent& event) override;
private:
    Listener* listener;
    
    ComboBox inputSourceBox;
    ComboBox inputChannelBox;
    
    CodeDocument document;
    PythonEditor editor;
    
    ToggleButton shouldSendNote;
    Label outputLabel;
    Slider outputDelay;
    
    TextButton pushCodeButton;
    
    ColourSelector colourSelector;
    TextButton colourSelectedButton;
    TextEditor tabName;
    
    ScriptStore* scriptStore;
    int scriptStoreId;
};

#endif /* CodingTab_hpp */
