//
//  GlobalVariableStore.hpp
//  MidiScript
//
//  Created by Adam Bethell on 13/10/2017.
//
//

#ifndef GlobalVariableStore_hpp
#define GlobalVariableStore_hpp

#include "../JuceLibraryCode/JuceHeader.h"
#include "GuiAreaDropZone.hpp"
#include "GuiValueMessage.hpp"

struct StringChangeMessage : public Message
{
    String oldStr;
    String newStr;
};

class GlobalVariableStore
{
public:
    GlobalVariableStore();
    ~GlobalVariableStore();
    
    static GlobalVariableStore* GetInstance() { return gvs; }
    
    unsigned int registerNewGlobal (const String& name, double value, GuiComponent* pointer);
    void updateGlobal (int id, const String& newName, double newValue, bool sendNotification);
    void unregisterGlobal (int id);
    double getValueForId (int id) const;
    void setIsMetro (int id);
        
    StringArray getStringArray()
    {
        const ScopedLock lock (criticalSection);
        return symbols;
    }
    Array<double> getValues()
    {
        const ScopedLock lock (criticalSection);
        return values;
    }
    Array<unsigned int> getIds()
    {
        const ScopedLock lock (criticalSection);
        return ids;
    }
    
    StringArray getMetros();
    
private:
    static GlobalVariableStore* gvs;
    CriticalSection criticalSection;
    
    
    StringArray symbols;
    Array<double> values;
    Array<unsigned int> ids;
    Array<GuiComponent*> pointers;
    Array<bool> isMetro;
    
    
    unsigned int idGen;
};

#endif /* GlobalVariableStore_hpp */
