//
//  GuiAreaDropZone.hpp
//  MidiScript
//
//  Created by Adam Bethell on 12/10/2017.
//
//

#ifndef GuiAreaDropZone_hpp
#define GuiAreaDropZone_hpp

#include "../JuceLibraryCode/JuceHeader.h"
#include "GuiSlider.hpp"
#include "GuiToggle.hpp"
#include "GuiMetronome.hpp"
#include "GlobalVariableStore.hpp"

class GuiAreaDropZone :
public Component,
public DragAndDropTarget,
public GuiComponent::Listener
{
public:
    GuiAreaDropZone();
    ~GuiAreaDropZone();
    
    void paint (Graphics& g) override;
    void resized() override;
    bool isInterestedInDragSource (const SourceDetails& dragSourceDetails) override;
    void itemDropped (const SourceDetails& dragSourceDetails) override;
    
    void stateUpdated (const String& name, double value, int id) override;
private:
    void addNewComponent (Point<int> pos, GuiComponent* newComponent);
        
    OwnedArray<GuiComponent> guiComponents;
};

#endif /* GuiAreaDropZone_hpp */
