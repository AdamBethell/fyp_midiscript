//
//  GuiMetronome.cpp
//  MidiScript
//
//  Created by Adam Bethell on 13/10/2017.
//
//

#include "GuiMetronome.hpp"

GuiMetronome::GuiMetronome() : Thread("Metronome Thread")
{
    name = "NewMetro";
    
    addAndMakeVisible(&onOffButton);
    onOffButton.setButtonText("Active");
    onOffButton.addListener(this);
    isOn.set(false);
    
    addAndMakeVisible(&bpmSlider);
    bpmSlider.setSliderStyle(Slider::SliderStyle::IncDecButtons);
    bpmSlider.setRange(1.0, 300.0, 0.1);
    bpmSlider.setValue(120.0, dontSendNotification);
    bpmSlider.addListener(this);
    
    addAndMakeVisible(&textEditor);
    textEditor.addListener(this);
    textEditor.setText(name, dontSendNotification);
    textEditor.setInputRestrictions(20, "1234567890abcdefghijklmnopqrstuvwxyz_ABCDEFGHIJKLMNOPQRSTUVWXYZ");
    
    sendOff = false;
    currentTime = Time::getMillisecondCounter();
    maxTime.set(60000/bpmSlider.getValue());
    startThread();
}
GuiMetronome::~GuiMetronome()
{
    signalThreadShouldExit();
    while (isThreadRunning()) {
        Thread::sleep(10);
    }
}

void GuiMetronome::paint (Graphics& g)
{
    g.setColour(Colours::black);
    g.fillRoundedRectangle(0, 0, getWidth(), getHeight(), 5);
    g.setColour(Colours::dimgrey);
    g.fillRoundedRectangle(1, 1, getWidth()-2, getHeight()-2, 5);
}
void GuiMetronome::resized()
{
    Rectangle<int> bounds = getLocalBounds();
    bounds.reduce(6, 6);
    
    onOffButton.setBounds (bounds.removeFromTop(30).reduced(20, 0));
    textEditor.setBounds(bounds.removeFromBottom(25));
    bpmSlider.setBounds (bounds);
    
}
void GuiMetronome::run()
{
    while (!threadShouldExit())
    {
        if (Time::getMillisecondCounter() - currentTime >= maxTime.get())
        {
            if (sendOff)
            {
                MetroMidiMessage* mmm = new MetroMidiMessage();
                mmm->message = MidiMessage::noteOff(1, 0, (uint8)0);
                mmm->source = name;
                PythonProcessor::GetInstance()->postMessage(mmm);
                
                sendOff = false;
            }
            if (isOn.get())
            {
                MetroMidiMessage* mmm = new MetroMidiMessage();
                mmm->message = MidiMessage::noteOn(1, 0, (uint8)1);
                mmm->source = name;
                if (PythonProcessor::GetInstance() == nullptr) jassertfalse;
                PythonProcessor::GetInstance()->postMessage(mmm);
                
                sendOff = true;
            }
            currentTime = Time::getMillisecondCounter();
        }
        Thread:sleep(1);
    }
}

void GuiMetronome::buttonClicked (Button* button)
{
    if (button == &onOffButton)
    {
        isOn.set(button->getToggleState());
    }
}
void GuiMetronome::sliderValueChanged (Slider* slider)
{

    notifyStateUpdated(String::empty, name, bpmSlider.getValue(), id);
    
    maxTime.set(60000/bpmSlider.getValue());
}

void GuiMetronome::textEditorTextChanged (TextEditor& editor)
{
    notifyStateUpdated(name, editor.getText(), bpmSlider.getValue(), id);
    name = editor.getText();
}

void GuiMetronome::handleMessage (const Message& message)
{
    double value = dynamic_cast<const GuiValueMessage*>(&message)->value;
    
    bpmSlider.setValue(value, dontSendNotification);
}

void GuiMetronome::setName (String newName)
{
    name = newName;
    textEditor.setText(name, dontSendNotification);
}
