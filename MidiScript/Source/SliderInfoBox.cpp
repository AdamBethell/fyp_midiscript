//
//  SliderInfoBox.cpp
//  MidiScript
//
//  Created by Adam Bethell on 12/10/2017.
//
//

#ifndef SliderInfoBox_hpp
#define SliderInfoBox_hpp

#include "../JuceLibraryCode/JuceHeader.h"

class SliderInfoBox : public Component, public Slider::Listener, public Button::Listener
{
public:
    SliderInfoBox()
    {
        addAndMakeVisible (&lowerBound);
        addAndMakeVisible (&upperBound);
        addAndMakeVisible (&intOnly);
        
        lowerBound.setSliderStyle (Slider::SliderStyle::IncDecButtons);
        upperBound.setSliderStyle (Slider::SliderStyle::IncDecButtons);
        
        intOnly.setToggleState (false, dontSendNotification);
        intOnly.setButtonText("Whole numbers only?");
        
        lowerBound.setRange (-100000, 100000, 1.0);
        upperBound.setRange (-100000, 100000, 1.0);
        
        lowerBound.addListener (this);
        upperBound.addListener (this);
        intOnly.addListener (this);
    }
    ~SliderInfoBox() {}
    
    void passRange (double lower, double upper)
    {
        lowerBound.setValue (lower, dontSendNotification);
        upperBound.setValue (upper, dontSendNotification);
    }
    void passIntOnly (bool integersOnly)
    {
        intOnly.setToggleState(integersOnly, dontSendNotification);
    }
    
    void resized() override
    {
        Rectangle<int> bounds = getLocalBounds();
        double third = bounds.getHeight()/3.0;
        lowerBound.setBounds (bounds.removeFromTop(third));
        upperBound.setBounds (bounds.removeFromTop(third));
        intOnly.setBounds (bounds);
    }
    void sliderValueChanged (Slider* slider) override
    {
        if (listener != nullptr)
        {
            listener->rangeChanged (lowerBound.getValue(), upperBound.getValue());
        }
    }
    void buttonClicked (Button* button) override
    {
        if (listener != nullptr)
        {
            listener->intOnlyChanged (intOnly.getToggleState());
            
            if (intOnly.getToggleState())
            {
                lowerBound.setRange (-100000, 100000, 1.0);
                upperBound.setRange (-100000, 100000, 1.0);
            }
            else
            {
                lowerBound.setRange (-100000, 100000, 0.001);
                upperBound.setRange (-100000, 100000, 0.001);
            }
        }
    }
    
    class Listener
    {
    public:
        virtual ~Listener(){}
        virtual void rangeChanged (double lower, double upper) = 0;
        virtual void intOnlyChanged (bool state) = 0;
    };
    void setListener (Listener* newListener) { listener = newListener; }
private:
    Listener* listener;
    Slider lowerBound;
    Slider upperBound;
    ToggleButton intOnly;
};

#endif /* SliderInfoBox_hpp */
