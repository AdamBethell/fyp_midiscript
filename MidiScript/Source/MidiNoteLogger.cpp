//
//  MidiNoteLogger.cpp
//  MidiScript
//
//  Created by Adam Bethell on 28/07/2017.
//
//

#include "MidiNoteLogger.hpp"

void MidiNoteLogger::logNewNote(MidiNoteLog log)
{
    noteLog.add(log);
}

MidiMessage MidiNoteLogger::getModifiedNote(const MidiMessage& noteOffMessage)
{
    for (int i = 0; i < noteLog.size(); i++)
    {
        if (noteOffMessage.getNoteNumber() == noteLog[i].rawNote && noteOffMessage.getChannel() == noteLog[i].rawChannel)
        {
            int channel = noteLog[i].modifiedChannel;
            int note = noteLog[i].modifiedNote;
            noteLog.remove(i);
            return MidiMessage::noteOff(channel, note, (uint8)0);
        }
    }
    DBG("MidiNoteLogger::getModifiedNote() Note was not found in log!");
    return noteOffMessage;
}
