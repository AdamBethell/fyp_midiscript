//
//  SmartNoteOffs.hpp
//  MidiScript
//
//  Created by Adam Bethell on 29/09/2017.
//
//

#ifndef SmartNoteOffs_hpp
#define SmartNoteOffs_hpp

#include "../JuceLibraryCode/JuceHeader.h"

class SmartNoteOffs
{
public:
    void logNote (const MidiMessage& noteOnMessage, const MidiBuffer& outputBuffer)
    {
        int index = findLoggedNote (noteOnMessage);
        if (index == -1)
        {
            log.add(Pair(noteOnMessage, outputBuffer));
        }
        else
        {
            log[index].addNotesOff(outputBuffer);
        }
    }
    MidiBuffer getOutputBufferForNoteOff (const MidiMessage& noteOffMessage)
    {
        int index = findLoggedNote (noteOffMessage);
        
        if (index == -1) return MidiBuffer();
        
        MidiBuffer bufferToSend = log[index].notesOff;
        log.remove(index);
        return bufferToSend;
    }
private:
    struct Pair
    {
        Pair() {}
        Pair (MidiMessage message, MidiBuffer buffer) : noteOn(message)
        {
            addNotesOff(buffer);
        }
        ~Pair() {}
        void addNotesOff (MidiBuffer midiBuffer)
        {
            MidiBuffer::Iterator iterator(midiBuffer);
            
            MidiMessage midiMessage;
            int samplePos;

            while (iterator.getNextEvent(midiMessage, samplePos))
            {
                notesOff.addEvent(MidiMessage::noteOff(midiMessage.getChannel(), midiMessage.getNoteNumber(), (uint8)0), samplePos);
            }
        }
        MidiMessage noteOn;
        MidiBuffer notesOff;
        
    };
    
    int findLoggedNote (const MidiMessage& noteMessage)
    {
        int note = noteMessage.getNoteNumber();
        int channel = noteMessage.getChannel();
        
        for (int i = 0; i < log.size(); i++)
        {
            if (log[i].noteOn.getNoteNumber() == note && log[i].noteOn.getChannel() == channel)
            {
                return i;
            }
        }
        return -1;
    }
    
    Array<Pair> log;
};

#endif /* SmartNoteOffs_hpp */
