//
//  PythonHandler.hpp
//  MidiScript
//
//  Created by Adam Bethell on 28/07/2017.
//
//

#ifndef PythonHandler_hpp
#define PythonHandler_hpp

#include "../JuceLibraryCode/JuceHeader.h"
#include "Python/Python.h"
#include "MidiNoteLogger.hpp"
#include "MidiNoteLog.hpp"

class PythonHandler
{
public:
    PythonHandler();
    ~PythonHandler();
    
    MidiMessage processMessage (const MidiMessage& message, String sourceName);
    void passScript (String script);
    void runOnce (String script);
    
private:
    void putPyObjectsToDict();
    void getPyObjectsFromDict();
    
    String scriptToRun;
    
    MidiNoteLogger noteLogger;
    
    
    PyObject* main;
    PyObject* dict;
    
    PyObject* noteNumber;
    PyObject* noteVelocity;
    PyObject* noteChannel;
    PyObject* sourceString;
};

#endif /* PythonHandler_hpp */
