/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#include "MainComponent.h"


//==============================================================================
MainContentComponent::MainContentComponent() :
pythonProcessor(&scriptStore),
scriptingArea(&scriptStore, &pythonProcessor),
guiTabbedArea(TabbedButtonBar::Orientation::TabsAtRight)
{
    setSize (1500, 800);
    addAndMakeVisible(&scriptingArea);
    addAndMakeVisible(&guiTabbedArea);
    guiTabbedArea.addTab("GUI", Colours::beige, new GuiAreaComponent(), true);
//    guiTabbedArea.addTab("Current script info", Colours::blanchedalmond, new ScriptInfoComponent(), true);
}

MainContentComponent::~MainContentComponent()
{
}

void MainContentComponent::paint (Graphics& g)
{
    g.fillAll (Colours::whitesmoke);
}

void MainContentComponent::resized()
{
    Rectangle<int> bounds = getBounds();
    scriptingArea.setBounds(bounds.removeFromLeft(750));
    guiTabbedArea.setBounds(bounds);
}


