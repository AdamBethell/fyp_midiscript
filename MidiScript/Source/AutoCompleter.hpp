//
//  AutoCompleter.hpp
//  MidiScript
//
//  Created by Adam Bethell on 28/09/2017.
//
//

#ifndef AutoCompleter_hpp
#define AutoCompleter_hpp

#include "../JuceLibraryCode/JuceHeader.h"
#include "GlobalVariableStore.hpp"

class AutoCompleter : public Component, public Button::Listener
{
public:
    AutoCompleter (String searchString);
    ~AutoCompleter();
    
    bool foundCompletions()
    {
        if (matchedCompletions.size() > 0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    
    void buttonClicked (Button* button) override;
    
    class Listener
    {
    public:
        virtual ~Listener(){}
        virtual void completionSelected (const String& completionCode) = 0;
    };
    void setListener (Listener* newListener) { listener = newListener; }
private:
    Listener* listener;
        
    struct Completion
    {
        Completion(){}
        Completion(String searchName, String searchDescription, String outputStringResult) :
        name(searchName),
        description(searchDescription),
        outputString(outputStringResult)
        {}
        
        String name;
        String description;
        String outputString;
    };
    
    static void updateCompletions();
    static void addStandardCompletions();
    static Array<Completion> completions;
    
    Array<const Completion*> getCompletionsForSearch (String& searchString);
    OwnedArray<TextButton> completionResults;
    Array<const Completion*> matchedCompletions;
};


#endif /* AutoCompleter_hpp */
