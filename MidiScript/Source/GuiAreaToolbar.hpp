//
//  GuiAreaToolbar.hpp
//  MidiScript
//
//  Created by Adam Bethell on 12/10/2017.
//
//

#ifndef GuiAreaToolbar_hpp
#define GuiAreaToolbar_hpp

#include "../JuceLibraryCode/JuceHeader.h"

class GuiAreaToolbar : public Component
{
public:
    GuiAreaToolbar();
    ~GuiAreaToolbar();
    
    void paint (Graphics& g) override;
    void resized() override;
private:
    class DragAndDropImage : public Component
    {
    public:
        DragAndDropImage ()
        {
            name = "NULL";
        }
        void setImage (File f)
        {
            image = ImageFileFormat::loadFrom(f);
        }
        
        void paint(Graphics& g) override
        {
            if (image.getHeight() > 0)
                g.drawImage(image, 0, 0, getLocalBounds().getWidth(), getLocalBounds().getHeight(), 0, 0, image.getWidth(), image.getHeight());
        }
        
        void mouseDown (const MouseEvent& event) override
        {
            DragAndDropContainer::findParentDragContainerFor(this)->startDragging(name, this);
        }
        void setName (String newName)
        {
            name = newName;
        }
    private:
        Image image;
        String name;
    };
    
    DragAndDropImage slider;
    DragAndDropImage toggle;
    DragAndDropImage metro;
    
    
};
#endif /* GuiAreaToolbar_hpp */
