//
//  GuiAreaComponent.cpp
//  MidiScript
//
//  Created by Adam Bethell on 03/10/2017.
//
//

#include "GuiAreaComponent.hpp"

GuiAreaComponent::GuiAreaComponent()
{
    addAndMakeVisible(&toolbar);
    addAndMakeVisible(&dropZone);
    
}
GuiAreaComponent::~GuiAreaComponent()
{
    
}

void GuiAreaComponent::resized ()
{
    Rectangle<int> bounds = getLocalBounds();
    toolbar.setBounds(bounds.removeFromTop(50));
    dropZone.setBounds(bounds.reduced(2));
}

