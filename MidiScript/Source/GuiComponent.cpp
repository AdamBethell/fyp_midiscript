//
//  GuiComponent.cpp
//  MidiScript
//
//  Created by Adam Bethell on 13/10/2017.
//
//

#ifndef GuiComponent_cpp
#define GuiComponent_cpp

#include "../JuceLibraryCode/JuceHeader.h"
#include "GuiValueMessage.hpp"

class GuiComponent : public Component, public MessageListener
{
public:
    Point<int> getPointOffset() { return pointOffset; }
    virtual String getVarName() = 0;
    virtual double getVarValue() = 0;
    virtual void setID (int idNum) = 0;
    virtual void setName (String name) = 0;
    void mouseDrag (const MouseEvent& event) override
    {
        DragAndDropContainer::findParentDragContainerFor(this)->startDragging("GuiComponent", this);
        pointOffset = event.getMouseDownPosition();
    }
    
    void notifyStateUpdated (String prevName, const String& name, double value, int id)
    {
        if (listener != nullptr)
        {
            listener->stateUpdated (name, value, id);
        }
        
    }
    class Listener
    {
    public:
        virtual ~Listener(){}
        virtual void stateUpdated (const String& name, double value, int id) = 0;
    };
    void setListener (Listener* newListener) { listener = newListener; }
protected:
    Listener* listener;
    Point<int> pointOffset;
};


#endif /* GuiComponent_cpp */
