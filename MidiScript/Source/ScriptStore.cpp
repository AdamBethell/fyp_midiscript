//
//  ScriptStore.cpp
//  MidiScript
//
//  Created by Adam Bethell on 02/08/2017.
//
//

#include "ScriptStore.hpp"

int ScriptStore::registerNewScript()
{
    const ScopedLock lock (criticalSection);
//    DBG("reg new ");
    
    idGen++;
    
    ids.add(idGen);
    scripts.add("");
    sources.add("");
    channels.add(0);
    
    return idGen;
}
void ScriptStore::unregisterScript (int id)
{
    const ScopedLock lock (criticalSection);
    
    for (int i = 0; i < ids.size(); i++)
    {
        if (id == ids[i])
        {
            ids.remove(i);
            scripts.remove(i);
            sources.remove(i);
            channels.remove(i);
            return;
        }
    }
}

// Change to updateSctipt AND source
void ScriptStore::updateScript (int id, String script)
{
//    DBG("update script");
    const ScopedLock lock (criticalSection);
    
    for (int i = 0; i < ids.size(); i++)
    {
        if (id == ids[i])
        {
            scripts.set(i, script);
            return;
        }
    }
}
void ScriptStore::updateSource (int id, String source, int channel)
{
//    DBG("update source");
    const ScopedLock lock (criticalSection);
    
    for (int i = 0; i < ids.size(); i++)
    {
        if (id == ids[i])
        {
            sources.set(i, source);
            channels.set(i, channel);
            return;
        }
    }
}

StringArray ScriptStore::getScriptsForSource (String source, int channel)
{
//    DBG("num scripts: " << sources.size());
    const ScopedLock lock (criticalSection);
    
    StringArray scriptsToReturn;
    for (int i = 0; i < sources.size(); i++)
    {
        if (source == sources[i] && (channel == channels[i] || channels[i] == 0))
        {
            scriptsToReturn.add(scripts[i]);
        }
    }
    return scriptsToReturn;
}
