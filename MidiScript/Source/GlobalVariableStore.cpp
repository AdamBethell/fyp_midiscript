//
//  GlobalVariableStore.cpp
//  MidiScript
//
//  Created by Adam Bethell on 13/10/2017.
//
//

#include "GlobalVariableStore.hpp"

GlobalVariableStore::GlobalVariableStore()
{
    jassert(gvs == nullptr);
    gvs = this;
    idGen = 0;
}
GlobalVariableStore::~GlobalVariableStore()
{
    
}

GlobalVariableStore* GlobalVariableStore::gvs = nullptr;

unsigned int GlobalVariableStore::registerNewGlobal (const String& name, double value, GuiComponent* pointer)
{
    const ScopedLock lock (criticalSection);
    if (symbols.contains(name))
    {
        return 0;
    }
    
    idGen++;
    
    symbols.add(name);
    values.add(value);
    ids.add(idGen);
    pointers.add(pointer);
    isMetro.add(false);
    
    return idGen;
}

void GlobalVariableStore::updateGlobal (int id, const String& newName, double newValue, bool sendNotification)
{
    const ScopedLock lock (criticalSection);
    for (int i = 0; i < ids.size(); i++)
    {
        if (ids[i] == id)
        {
            if (symbols[i] != newName)
            {
                // Notify scripts
            }
            symbols.set(i, newName);
            values.set(i, newValue);
            
            // Notify slider
            if (sendNotification)
            {
                GuiValueMessage* message = new GuiValueMessage();
                message->value = newValue;
                pointers[i]->postMessage(message);
            }
            
            
            break;
        }
    }
    
    
}

void GlobalVariableStore::unregisterGlobal(int id)
{
    const ScopedLock lock (criticalSection);
    for (int i = 0; i < ids.size(); i++)
    {
        if (ids[i] == id)
        {
            symbols.remove(i);
            values.remove(i);
            ids.remove(i);
            isMetro.remove(i);
            break;
        }
    }
}

double GlobalVariableStore::getValueForId (int id) const
{
    const ScopedLock lock (criticalSection);
    for (int i = 0; i < ids.size(); i++)
    {
        if (ids[i] == id)
        {
            return values[i];
        }
    }
    return {};
}

void GlobalVariableStore::setIsMetro (int id)
{
    const ScopedLock lock (criticalSection);
    for (int i = 0; i < ids.size(); i++)
    {
        if (ids[i] == id)
        {
            isMetro.set(i, true);
        }
    }
}

StringArray GlobalVariableStore::getMetros()
{
    const ScopedLock lock (criticalSection);
    
    StringArray sa;
    
    for (int i = 0; i < ids.size(); i++)
    {
        if (isMetro[i])
        {
            sa.add(symbols[i]);
        }
    }
    
    return sa;
}
