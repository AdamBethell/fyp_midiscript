//
//  ScriptingAreaComponent.hpp
//  MidiScript
//
//  Created by Adam Bethell on 29/09/2017.
//
//

#ifndef ScriptingAreaComponent_hpp
#define ScriptingAreaComponent_hpp

#include "../JuceLibraryCode/JuceHeader.h"
#include "CodingTab.hpp"
#include "StaticVariablesComponent.hpp"
#include "PythonProcessor.hpp"

class ScriptingAreaComponent :
public Component,
public Button::Listener,
public CodingTab::Listener,
public StaticVariablesComponent::Listener
{
public:
    ScriptingAreaComponent (ScriptStore* scriptStorePointer, PythonProcessor* pythonProcessor);
    ~ScriptingAreaComponent();
    
    static ScriptingAreaComponent* getInstance() { return THIS; }
    
    void resized() override;
    
    void buttonClicked (Button* button) override;
    
    void newColourSelected (Colour colour) override;
    void newTabNameChosen (String tabName) override;
    void initStaticVars (String script) override;
private:
    static ScriptingAreaComponent* THIS;
    
    // Coding Area
    void addNewPage ();
    TabbedComponent tabbedCodingArea;
    OwnedArray<CodingTab> codingTabs;
    TextButton newTabButton;
    TextButton deleteTabButton;
    // Static variables
    StaticVariablesComponent staticCode;
    
    ScriptStore* scriptStore;
    PythonProcessor* processor;
    
};
#endif /* ScriptingAreaComponent_hpp */
