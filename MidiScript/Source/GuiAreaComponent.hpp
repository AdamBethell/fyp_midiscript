//
//  GuiAreaComponent.hpp
//  MidiScript
//
//  Created by Adam Bethell on 03/10/2017.
//
//

#ifndef GuiAreaComponent_hpp
#define GuiAreaComponent_hpp

#include "../JuceLibraryCode/JuceHeader.h"
#include "GuiAreaToolbar.hpp"
#include "GuiAreaDropZone.hpp"

class GuiAreaComponent :
public Component,
public DragAndDropContainer
{
public:
    GuiAreaComponent();
    ~GuiAreaComponent();
    
    void resized () override;
private:
    GuiAreaToolbar toolbar;
    GuiAreaDropZone dropZone;
};

#endif /* GuiAreaComponent_hpp */
