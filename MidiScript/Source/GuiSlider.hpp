//
//  GuiSlider.hpp
//  MidiScript
//
//  Created by Adam Bethell on 12/10/2017.
//
//

#ifndef GuiSlider_hpp
#define GuiSlider_hpp

#include "../JuceLibraryCode/JuceHeader.h"
#include "SliderInfoBox.cpp"
#include "GuiComponent.cpp"


class GuiSlider :
public GuiComponent,
public Button::Listener,
public Slider::Listener,
public SliderInfoBox::Listener,
public TextEditor::Listener
{
public:
    GuiSlider();
    ~GuiSlider();
    
    void paint (Graphics& g) override;
    void resized() override;
    
    
    void buttonClicked (Button* button) override;
    void sliderValueChanged (Slider* slider) override;
    
    void rangeChanged (double lower, double upper) override;
    void intOnlyChanged (bool state) override;
    void textEditorTextChanged (TextEditor& textEditor) override;
    
    void setID (int idNum) override { id = idNum; }
    String getVarName() override { return name; }
    double getVarValue() override { return slider.getValue(); }
    void setName (String newName) override;
    
    void handleMessage (const Message& message) override;
    
    
private:
    TextButton infoButton;
    Slider slider;
    double sliderLower;
    double sliderUpper;
    bool intOnly;
    TextEditor nameInput;
    String name;
    Point<int> pointOffset;
    
    int id;
};

#endif /* GuiSlider_hpp */
