//
//  ScriptStore.hpp
//  MidiScript
//
//  Created by Adam Bethell on 02/08/2017.
//
//

#ifndef ScriptStore_hpp
#define ScriptStore_hpp

#include "../JuceLibraryCode/JuceHeader.h"

class ScriptStore : public CriticalSection
{
public:
//    ScriptStore() { DBG("scriptStore init"); }
//    ~ScriptStore() {}
    
    int registerNewScript();
    void unregisterScript (int id);
    void updateScript (int id, String script);
    void updateSource (int id, String source, int channel);
    StringArray getScriptsForSource (String source, int channel);
private:
    CriticalSection criticalSection;
    Array<int> ids;
    StringArray scripts;
    StringArray sources;
    Array<int> channels;
    unsigned int idGen;
};

#endif /* ScriptStore_hpp */
