//
//  SlideToggleSwitch.hpp
//  MidiScript
//
//  Created by Adam Bethell on 28/07/2017.
//
//

#ifndef SlideToggleSwitch_hpp
#define SlideToggleSwitch_hpp

#include "../JuceLibraryCode/JuceHeader.h"

class SlideToggleSwitch : public Component
{
public:
    SlideToggleSwitch();
    ~SlideToggleSwitch();
    
    void paint (Graphics& g) override;
    void mouseDown (const MouseEvent& event) override;
    
    bool getState () { return state; }
    
    class Listener
    {
    public:
        virtual ~Listener() {}
        virtual void stateChanged (bool state) = 0;
    };
    void setListener (Listener* newListener) { listener = newListener; }
private:
    
    
    Listener* listener;
    bool state;
};

#endif /* SlideToggleSwitch_hpp */
