//
//  GuiSlider.cpp
//  MidiScript
//
//  Created by Adam Bethell on 12/10/2017.
//
//

#include "GuiSlider.hpp"

GuiSlider::GuiSlider()
{
    addAndMakeVisible(&slider);
    slider.setRange(0.0, 127.0);
    slider.addListener(this);
    sliderLower = 0.0;
    sliderUpper = 127.0;
    intOnly = true;
    name = "NewSlider";
    
    addAndMakeVisible(&infoButton);
    infoButton.setButtonText("i");
    infoButton.addListener(this);
    infoButton.setBounds(0, 0, 20, 20);
    
    addAndMakeVisible(&nameInput);
    nameInput.setInputRestrictions(20, "1234567890abcdefghijklmnopqrstuvwxyz_ABCDEFGHIJKLMNOPQRSTUVWXYZ");
    nameInput.setText(name, dontSendNotification);
    nameInput.addListener(this);
    
}
GuiSlider::~GuiSlider()
{

}

void GuiSlider::paint (Graphics& g)
{
    g.setColour(Colours::black);
    g.fillRoundedRectangle(0, 0, getWidth(), getHeight(), 5);
    g.setColour(Colours::dimgrey);
    g.fillRoundedRectangle(1, 1, getWidth()-2, getHeight()-2, 5);
}
void GuiSlider::resized()
{
    Rectangle<int> bounds = getLocalBounds();
    bounds.reduce(2, 2);
    bounds.removeFromTop(20);
    infoButton.setBounds(getWidth()-22, 2, 20, 20);
    
    slider.setBounds(bounds.removeFromTop(bounds.getHeight()/2));
    nameInput.setBounds(bounds.reduced(bounds.getWidth()/4, 0));
    
}

void GuiSlider::buttonClicked(Button* button)
{
    SliderInfoBox* sliderInfoBox = new SliderInfoBox();
    sliderInfoBox->setSize(150, 100);
    sliderInfoBox->passRange(sliderLower, sliderUpper);
    sliderInfoBox->passIntOnly(intOnly);
    sliderInfoBox->setListener(this);
    
    CallOutBox::launchAsynchronously(sliderInfoBox, getScreenBounds(), nullptr);
}

void GuiSlider::sliderValueChanged (Slider* pSlider)
{
    if (intOnly)
    {
        pSlider->setValue((int)pSlider->getValue(), dontSendNotification);
    }
    
    if (listener != nullptr)
    {
        listener->stateUpdated(name, slider.getValue(), id);
    }
}

void GuiSlider::rangeChanged (double lower, double upper)
{
    slider.setRange(lower, upper);
    sliderLower = lower;
    sliderUpper = upper;
}
void GuiSlider::intOnlyChanged(bool state)
{
    intOnly = state;
}
void GuiSlider::textEditorTextChanged (TextEditor& textEditor)
{
    notifyStateUpdated(name, textEditor.getText(), slider.getValue(), id);
    name = textEditor.getText();
}

void GuiSlider::handleMessage (const Message& message)
{
    double value = dynamic_cast<const GuiValueMessage*>(&message)->value;
    slider.setValue(value, dontSendNotification);
}

void GuiSlider::setName (String newName)
{
    name = newName;
    nameInput.setText(name, dontSendNotification);
}
