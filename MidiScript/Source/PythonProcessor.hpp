//
//  PythonProcessor.hpp
//  MidiScript
//
//  Created by Adam Bethell on 04/08/2017.
//
//

#ifndef PythonProcessor_hpp
#define PythonProcessor_hpp

#include "../JuceLibraryCode/JuceHeader.h"
#include "Python/Python.h"
#include "ScriptStore.hpp"
#include "SmartNoteOffs.hpp"
#include "GlobalVariableStore.hpp"
#include "MetroMidiMessage.hpp"

class PythonProcessor : public MidiInputCallback, public MessageListener, public Timer
{
public:
    PythonProcessor(ScriptStore* scriptStorePointer);
    ~PythonProcessor();
    
    static PythonProcessor* GetInstance()
    {
        return pp;
    }
    
    void handleIncomingMidiMessage (MidiInput* source, const MidiMessage& message) override;
    void handleMessage (const Message& message) override;
    void messageHandler (String sourceName, const MidiMessage& message);
    void timerCallback() override;
    
    void runOnce (String script)
    {
        const ScopedLock lock (processorLock);
        
        Py_Finalize();
        Py_Initialize();
        Py_DecRef(main);
        Py_DecRef(dict);
        main = PyImport_ImportModule("__main__");
        dict = PyModule_GetDict(main);
        
//        Py_DecRef(main);
//        Py_DecRef(dict);
//        main = PyImport_ImportModule("__main__");
//        dict = PyModule_GetDict(main);
        
        PyRun_SimpleString(script.toRawUTF8());
    }
private:
    static PythonProcessor* pp;
    
    CriticalSection processorLock;
    
    void populateDict (PyObject* dict, StringArray strings, Array<double> values);
    void updateFromDict (PyObject* dict, StringArray strings, Array<double> values, Array<unsigned int> ids);
    
    PyObject* main;
    PyObject* dict;
    
    String imports;
    ScriptStore* scriptStore;
    SmartNoteOffs midiLogger;
    AudioDeviceManager audioDeviceManager;
    ScopedPointer<MidiOutput> midiOutput;
    
    StringArray currentInputs;
};

#endif /* PythonProcessor_hpp */
