//
//  GuiAreaToolbar.cpp
//  MidiScript
//
//  Created by Adam Bethell on 12/10/2017.
//
//

#include "GuiAreaToolbar.hpp"

GuiAreaToolbar::GuiAreaToolbar()
{
    addAndMakeVisible(&slider);
    slider.setImage(File::getCurrentWorkingDirectory().getChildFile("slidericon.png").getFullPathName());
    slider.setName("New Slider");
    
    addAndMakeVisible(&toggle);
    toggle.setImage(File::getCurrentWorkingDirectory().getChildFile("toggleicon.png").getFullPathName());
    toggle.setName("New Toggle");
    
    addAndMakeVisible(&metro);
    metro.setImage(File::getCurrentWorkingDirectory().getChildFile("metroicon.png").getFullPathName());
    metro.setName("New Metro");
}
GuiAreaToolbar::~GuiAreaToolbar()
{
    
}

void GuiAreaToolbar::paint (Graphics& g)
{
    g.fillAll(Colours::lightgrey);
}
void GuiAreaToolbar::resized()
{
    Rectangle<int> bounds = getLocalBounds();
    slider.setBounds(0, 0, bounds.getHeight(), bounds.getHeight());
    toggle.setBounds(bounds.getHeight(), 0, bounds.getHeight(), bounds.getHeight());
    metro.setBounds(bounds.getHeight() * 2, 0, bounds.getHeight(), bounds.getHeight());

}
