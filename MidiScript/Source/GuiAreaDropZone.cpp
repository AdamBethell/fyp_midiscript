//
//  GuiAreaDropZone.cpp
//  MidiScript
//
//  Created by Adam Bethell on 12/10/2017.
//
//

#include "GuiAreaDropZone.hpp"

GuiAreaDropZone::GuiAreaDropZone()
{
}
GuiAreaDropZone::~GuiAreaDropZone()
{
    
}

void GuiAreaDropZone::paint (Graphics& g)
{
    g.fillAll(Colours::grey);
}
void GuiAreaDropZone::resized()
{
    
}
bool GuiAreaDropZone::isInterestedInDragSource (const SourceDetails& dragSourceDetails)
{
    return true;
}

void GuiAreaDropZone::itemDropped (const SourceDetails& dragSourceDetails)
{
//    DBG("itemDropped(): " << dragSourceDetails.description.toString());
    if (dragSourceDetails.description == "New Slider")
    {
        GuiSlider* gs = new GuiSlider();
        gs->setBounds(0, 0, 350, 60);
        addNewComponent(dragSourceDetails.localPosition, gs);
    }
    else if (dragSourceDetails.description == "New Toggle")
    {
        GuiToggle* gt = new GuiToggle();
        gt->setBounds(0, 0, 150, 60);
        addNewComponent(dragSourceDetails.localPosition, gt);
    }
    else if (dragSourceDetails.description == "New Metro")
    {
        GuiMetronome* gm = new GuiMetronome();
        gm->setBounds (0, 0, 110, 110);
        addNewComponent (dragSourceDetails.localPosition, gm);
        GlobalVariableStore::GetInstance()->setIsMetro(gm->getId());
    }
    else if (dragSourceDetails.description == "GuiComponent")
    {
        Point<int> point = dragSourceDetails.localPosition;
        GuiComponent* component = dynamic_cast<GuiComponent*>(dragSourceDetails.sourceComponent.get());
        if (component == nullptr)
        {
            DBG("component == nullptr");
            return;
        }
        
        point.addXY (-component->getPointOffset().x, -component->getPointOffset().y);
        component->setTopLeftPosition (point);
    }
}

void GuiAreaDropZone::addNewComponent (Point<int> pos, GuiComponent* newComponent)
{
    addAndMakeVisible (newComponent);
    newComponent->setTopLeftPosition (pos);
    
    int id = 0;
    do
    {
        newComponent->setName (newComponent->getVarName() + "0");
        id = GlobalVariableStore::GetInstance()->registerNewGlobal (newComponent->getVarName(), newComponent->getVarValue(), newComponent);
    } while (id == 0);
    
    newComponent->setID (id);
    newComponent->setListener (this);
    guiComponents.add (newComponent);
    
    repaint();
}

void GuiAreaDropZone::stateUpdated (const String& name, double value, int id)
{
    GlobalVariableStore::GetInstance()->updateGlobal(id, name, value, false);
}
