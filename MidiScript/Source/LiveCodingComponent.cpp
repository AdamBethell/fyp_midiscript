//
//  LiveCodingComponent.cpp
//  MidiScript
//
//  Created by Adam Bethell on 28/07/2017.
//
//

#include "LiveCodingComponent.hpp"

LiveCodingComponent::LiveCodingComponent() : codeEditor(codeDocument, nullptr)
{
    addAndMakeVisible(&codeEditor);
    
    addAndMakeVisible(&submitCodeButton);
    submitCodeButton.addListener(this);
    submitCodeButton.setButtonText("Push code to midi IO");
    
    addAndMakeVisible(&infoLabel);
    infoLabel.setText("note, velocity, channel, source", dontSendNotification);
    infoLabel.setJustificationType(Justification::centred);
        
}

void LiveCodingComponent::resized()
{
    infoLabel.setBounds(0, 0, getWidth(), 25);
    codeEditor.setBounds(0, 25, getWidth(), getHeight()-75);
    submitCodeButton.setBounds(0, getHeight()-50, getWidth(), 50);
}

void LiveCodingComponent::buttonClicked (Button* button)
{
    if (button == &submitCodeButton)
    {
        if (listener != nullptr)
        {
            listener->scriptPushed(codeDocument.getAllContent());
        }
    }
}
