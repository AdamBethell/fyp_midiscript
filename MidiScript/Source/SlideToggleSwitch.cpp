//
//  SlideToggleSwitch.cpp
//  MidiScript
//
//  Created by Adam Bethell on 28/07/2017.
//
//

#include "SlideToggleSwitch.hpp"

SlideToggleSwitch::SlideToggleSwitch()
{
    
}
SlideToggleSwitch::~SlideToggleSwitch()
{
    
}


void SlideToggleSwitch::paint(Graphics& g)
{
    g.setColour(Colours::lightgrey);
    
    Rectangle<float> bounds = Rectangle<float>(0, 0, getWidth(), getHeight());
    bounds.reduce(2, 2);
    
    g.fillRoundedRectangle(bounds, 5);
    
    g.setColour(Colours::grey);
    g.drawRoundedRectangle(bounds, 5, 4);
    
    bounds.reduce(2, 2);
    if (state)
    {
        g.setColour(Colours::darkgrey);
        g.drawText("on", bounds.getX(), bounds.getY(), bounds.getWidth()/2, bounds.getHeight(), Justification::centred);
        g.fillRoundedRectangle(bounds.getWidth()/2 + bounds.getX(), bounds.getY(), bounds.getWidth()/2, bounds.getHeight(), 2.5);
    }
    else
    {
        g.setColour(Colours::darkgrey);
        g.drawText("off", bounds.getWidth()/2, bounds.getY(), bounds.getWidth()/2, bounds.getHeight(), Justification::centred);
        g.fillRoundedRectangle(bounds.getX(), bounds.getY(), bounds.getWidth()/2, bounds.getHeight(), 2.5);
        
    }
    
    
}

void SlideToggleSwitch::mouseDown(const MouseEvent& event)
{
    state = !state;
    
    if (listener != nullptr) listener->stateChanged(state);
    
    repaint();
}
