//
//  GuiMetronome.hpp
//  MidiScript
//
//  Created by Adam Bethell on 13/10/2017.
//
//

#ifndef GuiMetronome_hpp
#define GuiMetronome_hpp

#include "../JuceLibraryCode/JuceHeader.h"
#include "GuiComponent.cpp"
#include "PythonProcessor.hpp"
#include "MetroMidiMessage.hpp"

class GuiMetronome :
public GuiComponent,
public Thread,
public Slider::Listener,
public Button::Listener,
public TextEditor::Listener
{
public:
    GuiMetronome();
    ~GuiMetronome();
    
    void paint (Graphics& g) override;
    void resized() override;
    void run() override;
    
    void buttonClicked (Button* button) override;
    void sliderValueChanged (Slider* slider) override;
    void textEditorTextChanged (TextEditor& editor) override;
    void handleMessage (const Message& message) override;
    
    void setID (int idNum) override { id = idNum; }
    int getId () { return id; }
    String getVarName() override { return name; }
    double getVarValue() override { return bpmSlider.getValue(); }
    void setName (String newName) override;
private:
//    PythonProcessor* processor;
    
    Slider bpmSlider;
    ToggleButton onOffButton;
    TextEditor textEditor;
    
    uint32 currentTime;
    Atomic<uint32> maxTime;
    Atomic<bool> isOn;
    bool sendOff;
    
    int id;
    String name;
};

#endif /* GuiMetronome_hpp */
