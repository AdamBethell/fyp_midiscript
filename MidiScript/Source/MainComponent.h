/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#pragma once

#include "../JuceLibraryCode/JuceHeader.h"
#include "ScriptingAreaComponent.hpp"
#include "GuiAreaComponent.hpp"
#include "ScriptStore.hpp"
#include "PythonProcessor.hpp"
#include "GlobalVariableStore.hpp"

//==============================================================================
/*
    This component lives inside our window, and this is where you should put all
    your controls and content.
*/
class MainContentComponent :
public Component
{
public:
    //==============================================================================
    MainContentComponent();
    ~MainContentComponent();

    void paint (Graphics&) override;
    void resized() override;
private:
    // Processor
    GlobalVariableStore globalVariableStore;
    ScriptStore scriptStore;
    PythonProcessor pythonProcessor;
    
    
    ScriptingAreaComponent scriptingArea;
    TabbedComponent guiTabbedArea;
    
    
    
    //==============================================================================
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (MainContentComponent)
};
