//
//  GuiToggle.cpp
//  MidiScript
//
//  Created by Adam Bethell on 13/10/2017.
//
//

#include "GuiToggle.hpp"

GuiToggle::GuiToggle()
{
    name = "NewToggle";
    
    addAndMakeVisible(&toggleButton);
    toggleButton.addListener(this);
    toggleButton.setButtonText("");
    
    addAndMakeVisible(&textEditor);
    textEditor.addListener(this);
    textEditor.setText(name, dontSendNotification);
    textEditor.setInputRestrictions(20, "1234567890abcdefghijklmnopqrstuvwxyz_ABCDEFGHIJKLMNOPQRSTUVWXYZ");
    
}
GuiToggle::~GuiToggle()
{
    
}

void GuiToggle::paint(Graphics& g)
{
    g.setColour(Colours::black);
    g.fillRoundedRectangle(0, 0, getWidth(), getHeight(), 5);
    g.setColour(Colours::dimgrey);
    g.fillRoundedRectangle(1, 1, getWidth()-2, getHeight()-2, 5);
}

void GuiToggle::resized()
{
    Rectangle<int> bounds = getLocalBounds();
    bounds.reduce(2, 14);
    toggleButton.setBounds(bounds.removeFromLeft(30).reduced(0, 3));
    textEditor.setBounds(bounds.reduced(3, 0));
}

void GuiToggle::buttonClicked(Button* button)
{
    if (listener != nullptr)
    {
        listener->stateUpdated(name, (int)toggleButton.getToggleState(), id);
    }
}
void GuiToggle::textEditorTextChanged (TextEditor& editor)
{
    notifyStateUpdated(name, editor.getText(), (int)toggleButton.getToggleState(), id);
    name = editor.getText();
}

void GuiToggle::handleMessage (const Message& message)
{
    double value = dynamic_cast<const GuiValueMessage*>(&message)->value;
   
    bool on = false;
    if (value > 0) on = true;
    toggleButton.setToggleState(on, dontSendNotification);
}

void GuiToggle::setName (String newName)
{
    name = newName;
    textEditor.setText(newName, dontSendNotification);
}
