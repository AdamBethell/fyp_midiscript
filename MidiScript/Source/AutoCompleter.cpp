//
//  AutoCompleter.cpp
//  MidiScript
//
//  Created by Adam Bethell on 28/09/2017.
//
//

#include "AutoCompleter.hpp"

// STATIC
void AutoCompleter::updateCompletions()
{
    completions.clear();
    addStandardCompletions();
    
    StringArray sa = GlobalVariableStore::GetInstance()->getStringArray();
    for (int i = 0; i < sa.size(); i++)
    {
        completions.add(Completion(sa[i], sa[i] + ": Gui variable", sa[i]));
    }
    
}

void AutoCompleter::addStandardCompletions()
{
    completions.add (Completion("if", "Insert an if statement", "if ____ == ____:\n\t# do something.."));
    completions.add (Completion("if", "Insert an if/else statement", "if ____ == ____:\n\t# do something..\n\belse:\n\t# do something.."));
    completions.add (Completion("else", "Insert an else statement", "else:\n\t# do something.."));
    completions.add (Completion("while", "Insert a while loop", "while ____:\n\t# do something.."));
    completions.add (Completion("note", "note: the note coming in", "note"));
    completions.add (Completion("velocity", "velocity: the velocity of the note coming in", "velocity"));
    completions.add (Completion("shouldOutput", "shouldOutput: if set to zero, does not output the note - overrides the Gui", "shouldOutput"));
    completions.add (Completion("outputDelay", "outputDelay: the delay to be applied upon ouput - overrides the Gui", "outputDelay"));
}

Array<AutoCompleter::Completion> AutoCompleter::completions;

AutoCompleter::AutoCompleter (String searchString)
{
//    DBG("Searching with: " << searchString);
    updateCompletions();
    
    searchString = searchString.toLowerCase();
    matchedCompletions = getCompletionsForSearch (searchString);
    for (int i = 0; i < matchedCompletions.size(); i++)
    {
        
        completionResults.add(new TextButton(matchedCompletions[i]->description));
        completionResults.getLast()->setBounds(0, (completionResults.size()-1)*25, 500, 25);
        completionResults.getLast()->addListener(this);
        completionResults.getLast()->setMouseClickGrabsKeyboardFocus(false);
        addAndMakeVisible(completionResults.getLast());
        
    }
    if (completionResults.size() == 0)
    {
        completionResults.add(new TextButton("No auto-completions found"));
        completionResults.getLast()->setEnabled(false);
        completionResults.getLast()->setBounds(0, (completionResults.size()-1)*25, 400, 25);
        addAndMakeVisible(completionResults.getLast());
    }
    
    setBounds(0, 0, 500, completionResults.size() * 25);
}
AutoCompleter::~AutoCompleter()
{
    
}

void AutoCompleter::buttonClicked (Button* button)
{
    const String& s = button->getButtonText();
    for (int i = 0; i < completions.size(); i++)
    {
        if (completions[i].description == s)
        {
            if (listener != nullptr)
            {
                listener->completionSelected(completions[i].outputString);
            }
            break;
        }
    }
    
    
}

Array<const AutoCompleter::Completion*> AutoCompleter::getCompletionsForSearch (String& searchString)
{
    Array<const Completion*> matchedCompletions;
    
    for (int i = 0; i < completions.size(); i++)
    {
        if (completions[i].name.startsWithIgnoreCase(searchString))
        {
            matchedCompletions.add(&completions.getReference(i));
        }
    }
    
    return matchedCompletions;
}
