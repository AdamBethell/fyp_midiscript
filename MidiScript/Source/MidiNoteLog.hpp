//
//  MidiNoteLog.hpp
//  MidiScript
//
//  Created by Adam Bethell on 28/07/2017.
//
//

#ifndef MidiNoteLog_hpp
#define MidiNoteLog_hpp

#include "../JuceLibraryCode/JuceHeader.h"

struct MidiNoteLog
{
public:
    int rawNote;
    int rawChannel;
    int modifiedNote;
    int modifiedChannel;
};

#endif /* MidiNoteLog_hpp */
