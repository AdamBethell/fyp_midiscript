//
//  MidiNoteLogger.hpp
//  MidiScript
//
//  Created by Adam Bethell on 28/07/2017.
//
//

#ifndef MidiNoteLogger_hpp
#define MidiNoteLogger_hpp

#include "../JuceLibraryCode/JuceHeader.h"
#include "MidiNoteLog.hpp"

class MidiNoteLogger
{
public:
    void logNewNote(MidiNoteLog log);
    void logModifiedNote (const MidiMessage& outputNoteOnMessage);
    
    MidiMessage getModifiedNote (const MidiMessage& noteOffNumber);
private:
    Array<MidiNoteLog> noteLog;
};


#endif /* MidiNoteLogger_hpp */
