//
//  MidiMetronome.hpp
//  MidiScript
//
//  Created by Adam Bethell on 28/07/2017.
//
//

#ifndef MidiMetronome_hpp
#define MidiMetronome_hpp

#include "../JuceLibraryCode/JuceHeader.h"

class MidiMetronome : public Thread
{
public:
    MidiMetronome();
    ~MidiMetronome();
    void setSpeed (float bpm);
    void setIsOn (bool isOn) { shouldSend.set(isOn); }
    class Listener
    {
    public:
        virtual ~Listener() {}
        virtual void metroMessageSent () = 0;
    };
    void setListener (Listener* newListener) { listener = newListener; }
private:
    Listener* listener;
    void run () override;
    Atomic<bool> shouldSend;
    Atomic<float> waitTime;
};

#endif /* MidiMetronome_hpp */
