//
//  MidiMetronome.cpp
//  MidiScript
//
//  Created by Adam Bethell on 28/07/2017.
//
//

#include "MidiMetronome.hpp"

MidiMetronome::MidiMetronome() : Thread ("MidiMetronomeThread")
{
    shouldSend = false;
    startThread();
}

MidiMetronome::~MidiMetronome()
{
    signalThreadShouldExit();
    while (!threadShouldExit()) {
        
    }
}

void MidiMetronome::setSpeed(float bpm)
{
    waitTime.set(60000.0f/bpm);
}

void MidiMetronome::run ()
{
    while (!threadShouldExit())
    {
        // DO SOMETHING
        if (shouldSend.get() && listener != nullptr)
        {
            listener->metroMessageSent();
        }
        Time::waitForMillisecondCounter(Time::getMillisecondCounter() + waitTime.get());
    }
}
