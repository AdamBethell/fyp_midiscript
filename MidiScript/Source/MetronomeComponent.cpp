//
//  MetronomeComponent.cpp
//  MidiScript
//
//  Created by Adam Bethell on 28/07/2017.
//
//

#include "MetronomeComponent.hpp"

MetronomeComponent::MetronomeComponent()
{
    addAndMakeVisible(&bpmSlider);
    bpmSlider.setSliderStyle(Slider::SliderStyle::IncDecButtons);
    bpmSlider.setRange(0.5, 300, 0.5);
    bpmSlider.setValue(120, sendNotification);
    bpmSlider.setTextValueSuffix(" bpm");
    bpmSlider.addListener(this);
    
    addAndMakeVisible(&onOffSwitch);
    onOffSwitch.setListener(this);
    
    metronome.setListener(this);
    
    // Not thread safe
    name = "Metronome";
}

void MetronomeComponent::resized()
{
    bpmSlider.setBounds(0, 0, getWidth()/2, getHeight());
    onOffSwitch.setBounds(getWidth()/2, 0, getWidth()/2, getHeight());
}

void MetronomeComponent::metroMessageSent()
{
    if (listener != nullptr)
    {
        listener->messageFromMetronome(name);
    }
}

void MetronomeComponent::stateChanged (bool state)
{
    metronome.setIsOn(state);
}

void MetronomeComponent::sliderValueChanged (Slider* slider)
{
    metronome.setSpeed(slider->getValue());
}
