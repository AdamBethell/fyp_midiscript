//
//  PythonHandler.cpp
//  MidiScript
//
//  Created by Adam Bethell on 28/07/2017.
//
//

#include "PythonHandler.hpp"

PythonHandler::PythonHandler()
{
    Py_Initialize();
    main = PyImport_ImportModule("__main__");
    dict = PyModule_GetDict(main);
    
    scriptToRun = "";
}

PythonHandler::~PythonHandler()
{
    Py_Finalize();
}

void PythonHandler::passScript(String script)
{
    scriptToRun = script;
}

MidiMessage PythonHandler::processMessage(const MidiMessage& message, String sourceName)
{
    // Initialise variables
    int noteNum = -1;
    int noteVel = -1;
    int noteChan = -1;
    MidiMessage modifiedNoteOff;
    MidiNoteLog noteLog;
    
    if (message.isNoteOnOrOff())
    {
        // Set variables from incoming message
        noteNum = message.getNoteNumber();
        noteVel = message.getVelocity();
        noteChan = message.getChannel();
        if (message.isNoteOff())
        {
            modifiedNoteOff = noteLogger.getModifiedNote(message);
        }

    }
    else
    {
        // MidiScript onlt currently handles note messages
        return message;
    }
    
    // Set PyObject values
    noteNumber = PyInt_FromLong(noteNum);
    noteVelocity = PyInt_FromLong(noteVel);
    noteChannel = PyInt_FromLong(noteChan);
    sourceString = PyString_FromString(sourceName.toRawUTF8());
    
    // Put PyObjects in dictionary
    putPyObjectsToDict();
    
    // Run script
    PyRun_SimpleString(scriptToRun.toRawUTF8());
    
    // Get values from dictionary
    getPyObjectsFromDict();
    
    // Get values back in variables
    noteNum = _PyInt_AsInt(noteNumber);
    noteVel = _PyInt_AsInt(noteVelocity);
    noteChan = _PyInt_AsInt(noteChannel);
    
    if (noteNum > 127) noteNum = 127;
    else if (noteNum < 0) noteNum = 0;
    if (noteVel > 127) noteVel = 127;
    else if (noteVel < 0) noteVel = 0;
    if (noteChan > 16) noteChan = 16;
    else if (noteChan < 1) noteChan = 1;
    
    // Remove reference, I don't actually know what I'm doing here
//    Py_DECREF(noteNumber);
//    Py_DECREF(noteVelocity);
//    Py_DECREF(noteChannel);
//    Py_DECREF(noteOffRaw);
//    Py_DECREF(noteOffModified);
    
    
    
    if (message.isNoteOn() || sourceName == "Metronome")
    {
        noteLog.modifiedNote = noteNum;
        noteLog.modifiedChannel = noteChan;
        noteLogger.logNewNote(noteLog);
        
        return MidiMessage::noteOn(noteChan, noteNum, (uint8)noteVel);
    }
    else
    {
        return modifiedNoteOff;
    }
    
}

void PythonHandler::putPyObjectsToDict()
{
    // Put PyObjects in dictionary
    PyDict_SetItemString(dict, "note", noteNumber);
    PyDict_SetItemString(dict, "velocity", noteVelocity);
    PyDict_SetItemString(dict, "channel", noteChannel);
    PyDict_SetItemString(dict, "source", sourceString);
}
void PythonHandler::getPyObjectsFromDict()
{
    // Get values from dictionary
    noteNumber = PyMapping_GetItemString(dict, "note");
    noteVelocity = PyMapping_GetItemString(dict, "velocity");
    noteChannel = PyMapping_GetItemString(dict, "channel");
}

void PythonHandler::runOnce (String script)
{
    PyRun_SimpleString(script.toRawUTF8());
}
