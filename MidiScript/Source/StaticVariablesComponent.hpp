//
//  StaticVariablesComponent.hpp
//  MidiScript
//
//  Created by Adam Bethell on 28/07/2017.
//
//

#ifndef StaticVariablesComponent_hpp
#define StaticVariablesComponent_hpp

#include "../JuceLibraryCode/JuceHeader.h"

class StaticVariablesComponent : public Component, public Button::Listener
{
public:
    StaticVariablesComponent();
    ~StaticVariablesComponent() {}
    void resized() override;
    void buttonClicked (Button* button) override;
    
    class Listener
    {
    public:
        virtual ~Listener() {}
        virtual void initStaticVars (String script) = 0;
    };
    void setListener (Listener* newListener) { listener = newListener; }
private:
    
    String validChars;
    
    Listener* listener;
    CodeDocument codeDocument;
    CodeEditorComponent codeEditor;
    
    TextButton initialiseButton;
    
};

#endif /* StaticVariablesComponent_hpp */
