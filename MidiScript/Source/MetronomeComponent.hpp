//
//  MetronomeComponent.hpp
//  MidiScript
//
//  Created by Adam Bethell on 28/07/2017.
//
//

#ifndef MetronomeComponent_hpp
#define MetronomeComponent_hpp

#include "../JuceLibraryCode/JuceHeader.h"
#include "SlideToggleSwitch.hpp"
#include "MidiMetronome.hpp"

class MetronomeComponent : public Component, public SlideToggleSwitch::Listener, public Slider::Listener, public MidiMetronome::Listener
{
public:
    MetronomeComponent();
    ~MetronomeComponent() {}
    
    void resized () override;
    void stateChanged (bool state) override;
    void sliderValueChanged (Slider* slider) override;
    void metroMessageSent () override;
    
    // Not thread safe
    void setName (String newName) { name = newName; }
    
    bool getState () { return onOffSwitch.getState(); }
    bool getBPM () { return bpmSlider.getValue(); }
    
    class Listener
    {
    public:
        virtual ~Listener() {}
        virtual void messageFromMetronome(String source) = 0;
    };
    void setListener(Listener* newListener) { listener = newListener; }
private:
    Listener* listener;
    
    // Not thread safe
    String name;
    MidiMetronome metronome;
    Slider bpmSlider;
    SlideToggleSwitch onOffSwitch;
    
};

#endif /* MetronomeComponent_hpp */
