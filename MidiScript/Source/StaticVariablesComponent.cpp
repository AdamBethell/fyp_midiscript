//
//  StaticVariablesComponent.cpp
//  MidiScript
//
//  Created by Adam Bethell on 28/07/2017.
//
//

#include "StaticVariablesComponent.hpp"

StaticVariablesComponent::StaticVariablesComponent() : codeEditor(codeDocument, nullptr)
{
    addAndMakeVisible(&codeEditor);
    
    addAndMakeVisible(&initialiseButton);
    initialiseButton.setButtonText("init");
    initialiseButton.addListener(this);
}

void StaticVariablesComponent::resized()
{
    codeEditor.setBounds(0, 0, getWidth(), getHeight()-50);
    initialiseButton.setBounds(getBounds().removeFromBottom(50).reduced(1));
}

void StaticVariablesComponent::buttonClicked(Button* button)
{
    if (button == &initialiseButton)
    {
        if (listener != nullptr)
        {
            listener->initStaticVars(codeDocument.getAllContent());
        }
    }
}
