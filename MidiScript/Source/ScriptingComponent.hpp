//
//  ScriptingComponent.hpp
//  MidiScript
//
//  Created by Adam Bethell on 28/07/2017.
//
//

#ifndef ScriptingComponent_hpp
#define ScriptingComponent_hpp

#include "../JuceLibraryCode/JuceHeader.h"
#include "LiveCodingComponent.hpp"
#include "StaticVariablesComponent.hpp"
#include "PythonHandler.hpp"
#include "MetronomeComponent.hpp"

class ScriptingComponent :  public Component,
                            public MidiInputCallback,
                            public LiveCodingComponent::Listener,
                            public StaticVariablesComponent::Listener,
                            public Button::Listener,
                            public MetronomeComponent::Listener
{
public:
    //==============================================================================
    ScriptingComponent();
    ~ScriptingComponent();
    
    void paint (Graphics&) override;
    void resized() override;
    void buttonClicked (Button* button) override;
    void handleIncomingMidiMessage (MidiInput* source, const MidiMessage& message) override;
    void scriptPushed (String script) override;
    void initStaticVars (String script) override;
    void messageFromMetronome(String source) override;
private:
    StaticVariablesComponent staticVariablesComponent;
    LiveCodingComponent liveCodingComponent;
    MetronomeComponent metronome;
    
    TextButton newPageButton;
    TextButton codeWizardButton;
    TextButton zoomInButton;
    TextButton zoomOutButton;
    TextButton deletePageButton;
    
    //===================================
    ScopedPointer<MidiInput> midiInput;
    ScopedPointer<MidiOutput> midiOutput;
    AudioDeviceManager audioDeviceManager;
    
    PythonHandler pythonHandler;
};

#endif /* ScriptingComponent_hpp */
