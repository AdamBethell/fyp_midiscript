//
//  PythonEditor.hpp
//  MidiScript
//
//  Created by Adam Bethell on 28/09/2017.
//
//

#ifndef PythonEditor_hpp
#define PythonEditor_hpp

#include "../JuceLibraryCode/JuceHeader.h"
#include "AutoCompleter.hpp"

class PythonEditor : public CodeEditorComponent, public AutoCompleter::Listener
{
public:
    PythonEditor (CodeDocument& document);
    ~PythonEditor();
    
    void handleEscapeKey() override;
    void handleReturnKey() override;
    void updateCaretPosition() override;
    void completionSelected (const String& completionCode) override;
private:
    bool calloutIsOpen;
    ScopedPointer<AutoCompleter> autoCompleter;
    ScopedPointer<CallOutBox> autoSuggestionsBox;
    
    String getCurrentWord();
    Range<int> getCurrentWordRange();
    bool equalsValidChar (const char* c);
    void insertFormattedTextAtClaret (String s);
};

#endif /* PythonEditor_hpp */
