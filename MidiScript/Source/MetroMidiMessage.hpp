//
//  MetroMidiMessage.hpp
//  MidiScript
//
//  Created by Adam Bethell on 13/10/2017.
//
//

#ifndef MetroMidiMessage_hpp
#define MetroMidiMessage_hpp

#include "../JuceLibraryCode/JuceHeader.h"

struct MetroMidiMessage : public Message
{
public:
    MidiMessage message;
    String source;
};

#endif /* MetroMidiMessage_hpp */
